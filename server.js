require('dotenv').config();
const express = require('express'); //parecido a un import
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
const port = process.env.PORT || 3000;  //especifica variable de entorno
const usersFile = require('./users.json');
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu4db/collections/';
//const apikey_mlab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF'
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;

  app.listen(port,function(){       //Funcion callback
    console.log('NodeJS escuchando en el puerto: ' + port);
    }
  );

  app.use(body_parser.json());  //Parsea y comprueba que lo que llega en el body es un json

//Operación GET con mLab (Collection)
app.get(URL_BASE + 'users',
function(req, res) {
  const httpClient = request_json.createClient(URL_mLab);
  console.log("Cliente HTTP mLab creado.");
  const fieldParam = 'f={"_id":0}&';
  httpClient.get('users?' + fieldParam + apikey_mlab,
    function(err, respuestaMLab, body) {
      console.log('Error: ' + err);
      console.log('Respuesta MLab: ' + respuestaMLab);
      console.log('Body: ' + body);
      var response = {};
      if(err) {
          response = {"msg" : "Error al recuperar users de mLab."}
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {"msg" : "Usuario no encontrado."};
          res.status(404);
        }
      }
      res.send(response);
    });
});

// GET users con ID (mLab)
// Postman 1: http://localhost:3001/techu/v2/users/2
  app.get(URL_BASE + 'users/:id',
    function(req,res){
      console.log("GET /api/v2/users/:id");
      console.log(req.params.id);
      let id = req.params.id; //id=7
//      let queryString = 'q={"id_user":' + id + '}&';
      let queryString = `q={"id_user": ${id} }&`;  //interpolación de expresiones (string template)
      let queryStrField = 'f={"_id":0}&';
      let httpClient = request_json.createClient(URL_mLab);
      //URL mLab que tenemos que construir
      //https://api.mlab.com/api/1/databases/techu4db/collections/users?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
      httpClient.get('users?' + queryString + queryStrField + apikey_mlab,
        function(error,res_mlab, data){
          console.log(`error: ${error}`);
          console.log(`res_mlab:` + res_mlab);
          console.log(`res_mlab: ${JSON.stringify(res_mlab)}`);
          console.log("Respuesta mLab correcta.");
          let response = {};
          if (error){
            response = {'msg':'Error en la petición a mLab'};
            res.status(500);
          } else {
            if(data.length > 0) {
              response = data;
            } else {
              response = {'msg':'Usuario no encontrado'};
              res.status(404);
            }
          }
          res.send(response);
        });
    });

//GET cuentas del usuario con parámetro ID - falta no sale en postman
  app.get(URL_BASE + 'users/:id/accounts', //GET a users/4/accounts
    function(req, res) {
      console.log("request.params.id: " + req.params.id);
      var id = req.params.id;  //se obtiene el parámetro que se pasa por la URL_BASE
      var queryStringID = 'q={"user_id":' + id + '}&'; //filtra usuario con el id filtrado
      var fieldString = 'f={"_id":0}&';  //interpolación de expresiones (string template)
      var ctas = 'f={"account":1, "_id":0}&';
      var httpClient = request_json.createClient(URL_mLab);
      httpClient.get('users?' + queryStringID + ctas + apikey_mlab,
        function(error, respuestaMLab, body){
          var respuesta = {}; //objeto vacío
          respuesta = !error ? body : {"msg":"Usuario no tiene cuentas"};   //si no hay error respuesta es igual a data, sino mandar mensaje
          res.send(respuesta);
      });
  });

// POST 'users' mLab
//Postman: http://localhost:3001/techu/v2/users
app.post(URL_BASE + 'users',
 function(req, res) {
  var  clienteMlab = request_json.createClient(URL_mLab);
  clienteMlab.get('users?'+ apikey_mlab,
    function(error, respuestaMLab , body) {
      let newID=body.length+1;
      console.log("newID:" + newID);
      var newUser = {
        "id" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };
      clienteMlab.post('users?' + apikey_mlab, newUser ,
       function(error, respuestaMLab, body) {
        res.send(body);
     });
  });
});

//PUT users mLab
  app.put(URL_BASE + 'users/:id',
    function(req, res) {
      var  clienteMlab = request_json.createClient(URL_mLab);
      clienteMlab.get('users?'+ apikey_mlab ,
        function(error, respuestaMLab , body) {
          console.log("newID:" + newID);
          newID=body.length+1;
          var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
          clienteMlab.put(URL_mLab + 'users?q={"id": ' + newID + '}&' + apikeyMLab, JSON.parse(cambio),
            function(error, respuestaMLab, body) {
              console.log("body:"+ body);
              res.send(body);
            });
    });
  });

//DELETE con mLab
  app.delete(URL_BASE + "users/:id",
    function(req, res){
      console.log("entra al DELETE");
      console.log("request.params.id: " +req.params.id);
      var id=req.params.id;
      var queryStringID='q={"id":' + id + '}&';
      console.log(URL_mLab + 'users?' + queryStringID + apikey_mlab);
      var httpClient = request_json.createClient(URL_mLab);
      httpClient.get('users?' +  queryStringID + apikey_mlab,
        function(error, respuestaMLab, body){
          var respuesta = body[0];
          console.log("body delete:"+ respuesta);
          httpClient.delete(URL_mLab + "users/"+ respuesta._id.$oid +'?'+ apikey_mlab,
            function(error, respuestaMLab, body){
              res.send(body);
            });
          });
    });

  //Method POST login
    app.post(URL_BASE + "login",
      function (req, res){
        console.log("POST /colapi/v2/login");
        let email = req.body.email;
        let pass = req.body.password;
        let queryStringEmail = 'q={"email":"' + email + '"}&';
        let queryStringPass = 'q={"password":' + pass + '}&';
        let clienteMlab = request_json.createClient(URL_mLab);
        console.log("email:. " + email);
        console.log("password: " +  pass);
        clienteMlab.get('users?'+ queryStringEmail + apikey_mlab,
          function(error, respuestaMLab, body) {
            var respuesta = body[0];
            console.log(respuesta);
            if(respuesta != undefined) {
              if (respuesta.password == pass) {
                console.log("Login OK");
                let session = {"logged":true};
                let login = '{"$set":' + JSON.stringify(session) + '}';
                clienteMlab.put('user?q={"id": ' + respuesta.id_user + '}&'+ apikey_mlab, JSON.parse(login),
                //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikey_mlab, JSON.parse(login),
                  function(errPut, resPut, bodyPut) {
                    res.send(respuesta);
                  });
              }
              else {
                res.status(404).send({"msg":"Contraseña errada"});
              }
            } else {
              res.status(404).send({"msg":"Email errado"});
            }
        });
    });

//Method POST logout
    app.post(URL_BASE + "logout",
      function(req, res) {
        console.log("POST /colapi/v2/logout");
        var email = req.body.email;
//        var queryStringEmail = 'q={"email":"' + email + '","logged":true}&';
        var queryStringEmail = 'q={"email":"' + email + '}&';
        console.log(queryStringEmail);
        var  clienteMlab = request_json.createClient(URL_mLab);
        clienteMlab.get('users?'+ queryStringEmail + apikey_mlab,
          function(error, respuestaMLab, body) {
            var respuesta = body[0];
            console.log(respuesta);
            if (respuesta != undefined) {
              console.log("Logout correcto");
              let session={"logged":true};
              let logout = '{"$unset":' + JSON.stringify(session) + '}';
              console.log(logout);
              clienteMlab.put('users?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
                resPut.send(body[0]);
              });
            } else {
                  res.status(404).send({"msg":"Error en logout"});
                }
        });
    });

//Petición GET a un único usuario mediante ID (instancia)
  app.get(URL_BASE + 'users/:id',  //Un sólo parámetro, el id puede ser cualquier nombre, no solo el nombre del campo
//app.get(URL_BASE + 'users/:id/:p1/:p2',   //Más de un parámetro
    function(request,response){
      console.log(request.params.id);  //Un sólo parámetro
//    console.log('id:' + request.params.id); //Varios parámetros
//    console.log('p1:' + request.params.p1);
//    console.log('p2:' + request.params.p2);
      let pos = request.params.id - 1; //let define variables de forma local, solo para la función
      let respuesta =
            (usersFile[pos] == undefined) ? {"msg": "Usuario no existente"} : usersFile[pos];
      let status_code = (usersFile[pos] == undefined) ? 404 : 200;
      response.status(status_code).send(respuesta);
    }
  );

//Get con query string (req.query)
  app.get(URL_BASE + 'usersq',
    function(req, res){
      console.log(req.query.id);
      console.log(req.query.email);
      let pos = req.params.id - 1;
      let respuesta =
            (usersFile[pos] == undefined) ? {"msg": "Usuario no existente"} : usersFile[pos];
      let status_code = (respuesta == undefined) ? 404 : 200;
      res.status(status_code);
      res.send(usersFile[pos - 1]);
      res.send({"msg": "GET con query"})
    });

//Petición POST a users
  app.post(URL_BASE + 'users',
    function(req, res){
      console.log('POST a users');
      let tam = usersFile.length;   //Devuelve el número de elementos que tiene el array
      let new_user = {
        "id_user": tam + 1,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
      }
      console.log(new_user);
      usersFile.push(new_user);
      res.send({"msg": "Usuario creado correctamente"});  //Se llama como se haya definido en la función (res)
    }
  );

// Petición PUT a users (Modificación de datos)
//    app.put(URL_BASE + 'users/:id',
//      function(req1, res2){
//        console.log('PUT a users');
//        let posicion = req1.params.ID - 1;

//        let modif_user = {
//          "first_name": req1.body.first_name,
//          "last_name": req1.body.last_name,
//          "email": req1.body.email,
//          "password": req1.body.password
//      }
//        console.log(modif_user);
//        usersFile[posicion] = modif_user;
//        res2.send({"msg":"Usuario actualizado"});
//      });

//Version PUT del profe
    app.put(URL_BASE + 'users/:id',
      function(req,res){
        console.log("PUT /techu/v1/users/:id");
        let idBuscar = req.params.id;
        let updateUser = req.body;
        for(i == 0; i < usersFile.length; i++){
          console.log(usersFile[i].id);
          if(usersFile[i].id == idBuscar){
            usersFile[i] = updateUser;
            res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
          }
        }
        res.send({"msg" : "Usuario no encontrado.", updateUser});
      });

//Petición DELETE a users (mediante su ID)
    app.delete(URL_BASE + 'users/:id',
      function(req, res){
        console.log('DELETE a users');
        let pos = req.params.id - 1;
        let delete_user = req.body;
        usersFile.splice(pos,1);
        res.send({"msg" : "Usuario eliminado"});
      });

// LOGIN - users.json
      app.post(URL_BASE + 'login',
        function(request, response) {   //callback
          console.log("POST /apicol/v2/login");
          console.log(request.body.email);
          console.log(request.body.password);
          var user = request.body.email;
          var pass = request.body.password;
          for(us of usersFile) {  //recorre el userFile
            if(us.email == user) {
              if(us.password == pass) {
                us.logged = true;
                writeUserDataToFile(usersFile);  //Funcion para escribir en userFile
                console.log("Login correcto!");
                response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
              } else {
                console.log("Login incorrecto.");
                response.send({"msg" : "Login incorrecto."});
              }
            }
          }
      });

      function writeUserDataToFile(data) {
        var fs = require('fs');
        var jsonUserData = JSON.stringify(data);
        fs.writeFile("./users.json", jsonUserData, "utf8",
        function(err) { //función manejadora para gestionar errores de escritura
          if(err) {
            console.log(err);
          } else {
            console.log("Datos escritos en 'users.json'.");
          }
        })
      };

// LOGOUT - users.json
      app.post(URL_BASE + 'logout',
        function(request, response) {
          console.log("POST /apicol/v2/logout");
          var userId = request.body.id;
          for(us of usersFile) {
            if(us.id == userId) {
              if(us.logged) {
                delete us.logged; // borramos propiedad 'logged'
                writeUserDataToFile(usersFile);
                console.log("Logout correcto!");
                response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
              } else {
                console.log("Logout incorrecto.");
                response.send({"msg" : "Logout incorrecto."});
              }
            }  us.logged = true
          }
      });
